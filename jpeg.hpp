#pragma once

#include "Image.hpp"
#include <iosfwd>

namespace imageio {    
    namespace jpeg {

        bool is_jpeg(std::istream &in);
        
        enum ColorSpace {
            JPEG_COLORSPACE_AUTO,
            JPEG_COLORSPACE_GRAY,
            JPEG_COLORSPACE_RGB,
        };
                         
        
        bool jpeg_read(std::istream& in, Image8& im,
                       ColorSpace cs = JPEG_COLORSPACE_AUTO);
        
        bool jpeg_write(const Image8& im, std::ostream& out,
                        int quality=98,
                        ColorSpace cs = JPEG_COLORSPACE_AUTO);
        
    }        
}
