#pragma once

#include "Image.hpp"
#include <iosfwd>

namespace imageio {    
    namespace png {                         

        bool is_png(std::istream &in);
        
        bool png_read(std::istream& in, Image8& im);        
        bool png_write(const Image8& im, std::ostream& out);
        
    }        
}
