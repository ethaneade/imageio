#include <iostream>
#include <ctype.h>
#include "pnm.hpp"
#include <stdint.h>
#include <vector>
#include <cassert>

using namespace imageio;
using namespace imageio::pnm;

static void skip_space(std::istream& in)
{            
    while (in.good() && isspace(in.get()));
    in.unget();            
}

static bool skip_comment(std::istream& in)
{
    char c = in.get();
    if (c != '#') {
        in.unget();
        return false;
    }
    while (in.good() && in.get() != '\n');
    return true;
}
        
static bool read_num(std::istream& in, int &n)
{
    char c = in.get();
    if (!isdigit(c))
        return false;
    n = c - '0';
    c = in.get();
            
    while (in.good() && isdigit(c)) {
        n = n*10 + (c - '0');
        c = in.get();
    }
    in.unget();
    return true;
}

struct Header {
    int fmt;
    int width, height, maxval;
};

static bool read_header(std::istream& in,
                        Header &header)
{
    char P = in.get();
    if (P != 'P') {
        in.unget();
        return false;
    }
        
    char fmt = in.get();
    switch (fmt) {
    case '2':
    case '3':
    case '5':
    case '6':
        break;
    default:
        in.unget();
        return false;
    }

    skip_space(in);
            
    header.fmt = fmt;
        
    int num[3];

    for (int i=0; i<3; ++i) {
        while (in.good() && skip_comment(in)) {}            
        skip_space(in);
        if (!read_num(in, num[i]))
            return false;
    }
    header.width = num[0];
    header.height = num[1];
    header.maxval = num[2];

    if (!isspace(in.get()))
        return false;
    return true;
}

static bool read_and_check_header(std::istream &in, Header &header)
{
    if (!read_header(in, header))
        return false;

    if (header.fmt != '2' && header.fmt != '5')
        return false;
        
    if (header.width < 0 || header.height < 0)
        return false;
        
    if (header.maxval < 1 || header.maxval > 65535)
        return false;
    
    return true;
}

bool imageio::pnm::get_pnm_type(std::istream &in, int &code)
{
    std::streambuf *s = in.rdbuf();
    char buf[2];
    int n = s->sgetn(buf, 2);
    for (int i=n-1; i>=0; --i)
        s->sputbackc(buf[i]);

    if (n != 2)
        return false;
    
    if (buf[0] != 'P')
        return false;

    char c = buf[1];

    switch (c) {
    case '2':
    case '3':
    case '5':
    case '6':
        code = c - '0';
        return true;
    default:
        return false;
    }
}

bool imageio::pnm::pgm_read(std::istream& in, Image8& im)
{
    Header header;
    if (!read_and_check_header(in, header))
        return false;

    im.resize(header.width, header.height, 1);
    if (header.fmt == '5') {
        if (header.maxval == 255) {
            for (int i=0; i<im.height; ++i) {
                in.read((char*)im.row(i), im.width);
                if (in.gcount() != im.width)
                    return false;
            }
            return true;
        }
        double factor = 255.0 / header.maxval;
        if (header.maxval > 255) {
            std::vector<uint16_t> rowbuf(im.width);
            for (int i=0; i<im.height; ++i) {
                in.read((char*)&rowbuf[0], im.width*2);
                if (in.gcount() != im.width*2)
                    return false;
                uint8_t *imi = im.row(i);
                for (int j=0; j<im.width; ++j)
                    imi[j] = (uint8_t)(rowbuf[j] * factor + 0.5);
            }
        } else {
            for (int i=0; i<im.height; ++i) {
                in.read((char*)im.row(i), im.width);
                if (in.gcount() != im.width)
                    return false;
                uint8_t * imi = im.row(i);
                for (int j=0; j<im.width; ++j)
                    imi[j] = (uint8_t)(imi[j] * factor + 0.5);
            }
        }
    } else if (header.fmt == '2') {
        double factor = 255.0 / header.maxval;
        for (int i=0; i<im.height; ++i) {
            uint8_t * imi = im.row(i);
            for (int j=0; j<im.width; ++j) {
                int val;
                if (!(in >> val))
                    return false;                    
                imi[j] = (uint8_t)(val * factor + 0.5);
            }
        }
    }
    return true;
}

bool imageio::pnm::pgm_read(std::istream& in, Image16& im)
{
    Header header;
    if (!read_and_check_header(in, header))
        return false;

    im.resize(header.width, header.height, 1);
    if (header.fmt == '5') {
        if (header.maxval == 65535) {
            for (int i=0; i<im.height; ++i) {
                in.read((char*)im.row(i), im.width*2);
                if (in.gcount() != im.width*2)
                    return false;
            }
            return true;
        }
        double factor = 65535.0 / header.maxval;
        if (header.maxval <= 255) {
            for (int i=0; i<im.height; ++i) {                
                uint8_t *r = (uint8_t*)im.row(i);
                in.read((char*)r, im.width);
                if (in.gcount() != im.width)
                    return false;
                uint16_t *imi = im.row(i);
                for (int j=im.width-1; j>=0; --j)
                    imi[j] = (uint8_t)(r[j] * factor + 0.5);
            }
        } else {
            for (int i=0; i<im.height; ++i) {
                in.read((char*)im.row(i), im.width*2);
                if (in.gcount() != im.width)
                    return false;
                uint16_t * imi = im.row(i);
                for (int j=0; j<im.width; ++j)
                    imi[j] = (uint8_t)(imi[j] * factor + 0.5);
            }
        }
    } else if (header.fmt == '2') {
        double factor = 65535.0 / header.maxval;
        for (int i=0; i<im.height; ++i) {
            uint16_t * imi = im.row(i);
            for (int j=0; j<im.width; ++j) {
                int val;
                if (!(in >> val))
                    return false;                    
                imi[j] = (uint8_t)(val * factor + 0.5);
            }
        }
    }
    return true;
}

bool imageio::pnm::pgm_write(const Image8& im, std::ostream& out)
{
    if (im.channels != 1)
        return false;
    
    out << "P5\n"        
        << im.width << " " << im.height << "\n"
        << "255\n";
    for (int i=0; i<im.height; ++i)
        out.write((const char*)im.row(i), im.width);

    return out.good();
}

bool imageio::pnm::pgm_write(const Image16& im, std::ostream& out)
{
    if (im.channels != 1)
        return false;
    
    out << "P5\n"
        << im.width << " " << im.height << "\n"
        << "65535\n";
    for (int i=0; i<im.height; ++i)
        out.write((const char*)im.row(i), im.width*2);

    return out.good();
}   


bool imageio::pnm::ppm_read(std::istream& in, Image8& im)
{
    Header header;
    if (!read_header(in, header))
        return false;

    if (header.fmt != '3' && header.fmt != '6')
        return false;
        
    if (header.width < 0 || header.height < 0)
        return false;
        
    if (header.maxval < 1 || header.maxval > 65535)
        return false;

    im.resize(header.width, header.height, 3);
    
    if (header.fmt == '6') {
        if (header.maxval == 255) {
            for (int i=0; i<im.height; ++i) {
                in.read((char*)im.row(i), im.width*3);
                if (in.gcount() != im.width*3)
                    return false;
            }
            return true;
        }
        double factor = 255.0 / header.maxval;
        if (header.maxval > 255) {
            std::vector<uint16_t> rowbuf(im.width*3);
            for (int i=0; i<im.height; ++i) {
                in.read((char*)&rowbuf[0], im.width*6);
                if (in.gcount() != im.width*6)
                    return false;
                uint8_t *imi = (uint8_t*)im.row(i);
                for (int j=0; j<im.width*3; ++j)
                    imi[j] = (uint8_t)(rowbuf[j] * factor + 0.5);
            }
        } else {
            for (int i=0; i<im.height; ++i) {
                in.read((char*)im.row(i), im.width*3);
                if (in.gcount() != im.width)
                    return false;
                uint8_t *imi = (uint8_t*)im.row(i);
                for (int j=0; j<im.width; ++j)
                    imi[j] = (uint8_t)(imi[j] * factor + 0.5);
            }
        }
    } else if (header.fmt == '3') {
        double factor = 255.0 / header.maxval;
        for (int i=0; i<im.height; ++i) {
            uint8_t * imi = im.row(i);
            for (int j=0; j<im.width; ++j) {
                int r, g, b;
                if (!(in >> r >> g >> b))
                    return false;                    
                imi[j] = (uint8_t)(r * factor + 0.5);
                imi[j+1] = (uint8_t)(g * factor + 0.5);
                imi[j+2] = (uint8_t)(b * factor + 0.5);
            }
        }
    }
    return true;
}

bool imageio::pnm::ppm_write(const Image8& im, std::ostream& out)
{
    if (im.channels != 3)
        return false;
    
    out << "P6\n"
        << im.width << " " << im.height << "\n"
        << "255\n";
    for (int i=0; i<im.height; ++i)
        out.write((const char*)im.row(i), im.width*3);

    return out.good();
}

