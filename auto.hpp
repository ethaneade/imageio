#pragma once

#include "Image.hpp"
#include <iosfwd>

namespace imageio {
    
    bool read(std::istream &in, Image8 &im);

    bool load(const char* filename, Image8 &im);

    struct SaveParams
    {
        int quality;

        SaveParams() {
            quality = 98;
        }
    };
        
    bool save(const Image8 &im, const char *filename, const SaveParams &params);    
    bool save(const Image8 &im, const char *filename);
}
