#pragma once

#include "Image.hpp"
#include <iosfwd>

namespace imageio {
    namespace pnm {

        // code is 2, 5, 3, or 6
        bool get_pnm_type(std::istream &in, int &code);
        
        bool pgm_read(std::istream& in, Image8& im);
        bool pgm_read(std::istream& in, Image16& im);
    
        bool pgm_write(const Image8& im, std::ostream& out);
        bool pgm_write(const Image16& im, std::ostream& out);

        bool ppm_read(std::istream& in, Image8& im);
        bool ppm_write(const Image8& im, std::ostream& out);
        
    }        
}
