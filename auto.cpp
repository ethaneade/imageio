#include <iostream>
#include <fstream>
#include "pnm.hpp"
#include "jpeg.hpp"
#include "png.hpp"
#include "auto.hpp"
#include <string.h>
#include <ctype.h>

using namespace imageio;

bool imageio::read(std::istream &in, Image8 &im)
{
    int code;
    if (pnm::get_pnm_type(in, code)) {
        switch (code) {
        case 2:
        case 5:
            return pnm::pgm_read(in, im);
        case 3:
        case 6:
            return pnm::ppm_read(in, im);
        default:
            return false;
        }
    }

    if (jpeg::is_jpeg(in)) {
        return jpeg::jpeg_read(in, im);
    }

    if (png::is_png(in)) {
        return png::png_read(in, im);
    }
        
    return false;
}

bool imageio::load(const char* filename, Image8 &im)
{
    std::ifstream in(filename, std::ios::binary);
    if (!in.good())
        return false;

    return read(in, im);
}

enum FileType {
    TYPE_UNKNOWN,
    TYPE_PGM,
    TYPE_PPM,
    TYPE_PNM,
    TYPE_JPEG,
    TYPE_PNG
};

static FileType get_file_type(const char *filename)
{
    int n = strlen(filename);
    if (n <= 3)
        return TYPE_UNKNOWN;
    int i = n-1;
    while (i>=0 && filename[i] != '.')
        --i;

    if (i == -1)
        return TYPE_UNKNOWN;
    ++i;

    int len = n - i;
    if (len > 4)
        return TYPE_UNKNOWN;
    
    char buf[5];
    for (int j=0; j<len; ++i, ++j)
        buf[j] = tolower(filename[i]);
    buf[len] = 0;
    
    if (!strcmp(buf, "pgm"))
        return TYPE_PGM;
    if (!strcmp(buf, "ppm"))
        return TYPE_PPM;
    if (!strcmp(buf, "pnm"))
        return TYPE_PNM;
    if (!strcmp(buf, "jpg"))
        return TYPE_JPEG;
    if (!strcmp(buf, "jpeg"))
        return TYPE_JPEG;
    if (!strcmp(buf, "png"))
        return TYPE_PNG;
    
    return TYPE_UNKNOWN;
}

bool imageio::save(const Image8 &im, const char *filename, const SaveParams &params)
{
    FileType type = get_file_type(filename);
    if (type == TYPE_UNKNOWN)
        type = TYPE_PNG;

    if (type == TYPE_PGM && im.channels != 1)
        return false;

    if (type == TYPE_PPM && im.channels != 3)
        return false;

    if (type == TYPE_PNM) {
        switch (im.channels) {
        case 1: type = TYPE_PGM; break;
        case 3: type = TYPE_PPM; break;
        default: return false;
        }
    }

    std::ofstream out(filename, std::ios::binary);
    if (!out.good())
        return false;
    
    switch (type) {
    case TYPE_PGM:
        return pnm::pgm_write(im, out);
    case TYPE_PPM:
        return pnm::ppm_write(im, out);
    case TYPE_JPEG:
        return jpeg::jpeg_write(im, out, params.quality);
    case TYPE_PNG:
        return png::png_write(im, out);
    default:
        return false;
    }
}

bool imageio::save(const Image8 &im, const char *filename)
{
    return save(im, filename, SaveParams());
}
