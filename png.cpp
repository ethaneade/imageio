#include <stdio.h>
#define PNG_DEBUG 0
#include <png.h>
#include <setjmp.h>

#include <iostream>
#include "png.hpp"
#include <vector>

using namespace imageio;

bool imageio::png::is_png(std::istream &in)
{
    std::streambuf *s = in.rdbuf();
    char header[8];
    int n = s->sgetn(header, 8);
    for (int i=n-1; i>=0; --i)
        s->sputbackc(header[i]);
    
    return 0 == png_sig_cmp((png_bytep)header, 0, n);
}

struct ReadState
{
    std::istream *in;
};

static void user_read_data(png_structp png_ptr,
                           png_bytep data, png_size_t length)
{
    ReadState *s = (ReadState*)png_get_io_ptr(png_ptr);
    s->in->read((char*)data, length);
}

bool imageio::png::png_read(std::istream& in, Image8& im)
{
    char header[8];    // 8 is the maximum size that can be checked

    in.read(header, 8);    
    if (0 != png_sig_cmp((png_bytep)header, 0, 8))
        return false;
    
    /* initialize stuff */
    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (!png_ptr)
        return false;

    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) {
        png_destroy_read_struct(&png_ptr, NULL, NULL);
        return false;
    }

    if (0 != setjmp(png_jmpbuf(png_ptr))) {
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        return false;
    }

    ReadState rs;
    rs.in = &in;
    png_set_read_fn(png_ptr, &rs, user_read_data);
    png_set_sig_bytes(png_ptr, 8);

    png_read_info(png_ptr, info_ptr);

    png_byte color_type = png_get_color_type(png_ptr, info_ptr);
    png_byte bit_depth = png_get_bit_depth(png_ptr, info_ptr);

    // Set options
    
    if (color_type == PNG_COLOR_TYPE_PALETTE)
        png_set_palette_to_rgb(png_ptr);
    
    if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
        png_set_tRNS_to_alpha(png_ptr);

    if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
        png_set_expand_gray_1_2_4_to_8(png_ptr);

    if (bit_depth == 16) {
#if PNG_LIBPNG_VER >= 10504
        png_set_scale_16(png_ptr);
#else
        png_set_strip_16(png_ptr);
#endif
    }
   
    /*int number_of_passes = */png_set_interlace_handling(png_ptr);
    png_read_update_info(png_ptr, info_ptr);

    // Alloc image
    int width = png_get_image_width(png_ptr, info_ptr);
    int height = png_get_image_height(png_ptr, info_ptr);
    int channels = png_get_channels(png_ptr, info_ptr);
    
    im.resize(width, height, channels);
    
    /* read file */
    if (0 != setjmp(png_jmpbuf(png_ptr)))
        return false;

    std::vector<png_bytep> row_pointers(im.height);
    for (int i=0; i<im.height; ++i)
        row_pointers[i] = (png_bytep)im.row(i);

    png_read_image(png_ptr, row_pointers.data());

    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    return true;
}

struct WriteState
{
    std::ostream *out;
};

static void user_write_data(png_structp png_ptr,
                            png_bytep data, png_size_t length)
{
    WriteState *s = (WriteState*)png_get_io_ptr(png_ptr);
    s->out->write((const char*)data, length);
}

static void user_flush_data(png_structp png_ptr)
{
    WriteState *s = (WriteState*)png_get_io_ptr(png_ptr);
    s->out->flush();
}

bool imageio::png::png_write(const Image8 &im, std::ostream &out)
{
    png_byte color_type;
    switch (im.channels) {
    case 1: color_type = PNG_COLOR_TYPE_GRAY; break;
    case 2: color_type = PNG_COLOR_TYPE_GRAY_ALPHA; break;
    case 3: color_type = PNG_COLOR_TYPE_RGB; break;
    case 4: color_type = PNG_COLOR_TYPE_RGB_ALPHA; break;
    default: return false;
    }
    
    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (!png_ptr)
        return false;

    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) {
        png_destroy_write_struct(&png_ptr, &info_ptr);
        return false;
    }
    
    if (0 != setjmp(png_jmpbuf(png_ptr))) {
        png_destroy_write_struct(&png_ptr, &info_ptr);
        return false;
    }
    
    WriteState ws;
    ws.out = &out;
    png_set_write_fn(png_ptr, &ws, user_write_data, user_flush_data);
    
    png_set_IHDR(png_ptr, info_ptr,
                 im.width, im.height, 8, color_type,
                 PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    png_write_info(png_ptr, info_ptr);

    std::vector<png_bytep> row_pointers(im.height);
    for (int i=0; i<im.height; ++i)
        row_pointers[i] = (png_bytep)im.row(i);
    
    png_write_image(png_ptr, row_pointers.data());
    
    png_write_end(png_ptr, NULL);    
    png_destroy_write_struct(&png_ptr, &info_ptr);

    return true;
}
