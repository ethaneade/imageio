#pragma once


namespace imageio {

    template <typename T>
    struct Image
    {
        int width, height;
        int stride;
        int channels;
        T *data;

        Image() {
            data = 0;
            dealloc();
        }

        Image(int w, int h, int c)
        {
            data = 0;
            dealloc();
            resize(w, h, c);
        }

        Image(T *data_, int w, int h, int s, int c)
            : width(w), height(h), stride(s), channels(c),
              data(data_)
        {
        }
        
        void resize(int w, int h, int c) {
            if (w == width && h == height && c == channels)
                return;
            dealloc();

            stride = w*c;
            data = new T[h*stride];
            
            width = w;
            height = h;
            channels = c;
        }
        
        void dealloc() {
            delete[] data;
            data = 0;
            width = height = 0;
            stride = 0;
            channels = 0;
        }

        T *row(int i) { return data + stride*i; }
        const T *row(int i) const { return data + stride*i; } 
    };

    typedef Image<unsigned char> Image8;
    typedef Image<unsigned short> Image16;

    template <typename T>
    struct ScopedImage : public Image<T>
    {
        ScopedImage() {}
        ~ScopedImage() { this->dealloc(); }
        
        ScopedImage(int w, int h, int c) : Image<T>(w,h,c) {}
        ScopedImage(T *d, int w, int h, int s, int c) : Image<T>(d,w,h,s,c) {}
    };
    
    typedef ScopedImage<unsigned char> ScopedImage8;
    typedef ScopedImage<unsigned short> ScopedImage16;

}
