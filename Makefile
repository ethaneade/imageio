CPPFLAGS=
CXXFLAGS=-g -Wall -O3

OBJS= 	pnm.o \
	jpeg.o \
	png.o \
	auto.o

all: libimageio.a
	+ make -C test

libimageio.a: $(OBJS)
	$(AR) rvs $@ $^


depend:
	$(RM) -f .deps
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -MM -MG -MP *.cpp *.hpp > .deps

clean:
	$(RM) $(OBJS) libimageio.a
	+ make -C test/ clean

-include .deps
