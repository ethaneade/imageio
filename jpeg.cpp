#include <iostream>
#include "jpeg.hpp"

#include <cstddef>
#include <jpeglib.h>
#include <setjmp.h>
#include <stdio.h>
#include <string.h>

using namespace imageio;
using namespace imageio::jpeg;

bool imageio::jpeg::is_jpeg(std::istream &in)
{
    std::streambuf *s = in.rdbuf();
    const int N = 11;
    unsigned char buf[N];
    int n = s->sgetn((char*)buf, N);
    for (int i=n-1; i>=0; --i)
        s->sputbackc(buf[i]);
    
    if (n != N)
        return false;
    
    return (buf[0] == 0xFF &&
            buf[1] == 0xD8 &&
            buf[2] == 0xFF &&
            buf[3] == 0xE0 &&
            buf[6] == 'J' &&
            buf[7] == 'F' &&
            buf[8] == 'I' &&
            buf[9] == 'F' &&
            buf[10] == 0);
}


struct jpeg_istream_mgr
{
    jpeg_source_mgr base;
    std::istream *in;

    static const int BUF_SIZE = 1024;
    JOCTET *buffer;
    
    static void init_source(j_decompress_ptr cinfo)
    {
        jpeg_istream_mgr *self = (jpeg_istream_mgr*)cinfo->src;
        self->buffer = new JOCTET[BUF_SIZE];
        self->base.next_input_byte = self->buffer;
        self->base.bytes_in_buffer = 0;
    }
    
    static boolean fill_input_buffer(j_decompress_ptr cinfo)
    {        
        jpeg_istream_mgr *self = (jpeg_istream_mgr*)cinfo->src;
        self->in->read((char*)self->buffer, BUF_SIZE);
        self->base.next_input_byte = self->buffer;
        self->base.bytes_in_buffer = self->in->gcount();
        return TRUE;
    }
    static void skip_input_data(j_decompress_ptr cinfo, long num_bytes)
    {
        jpeg_istream_mgr *self = (jpeg_istream_mgr*)cinfo->src;
        if (num_bytes >= (long)self->base.bytes_in_buffer) {
            num_bytes -= self->base.bytes_in_buffer;
            self->base.bytes_in_buffer = 0;
            self->in->ignore(num_bytes);
        } else {
            self->base.next_input_byte += num_bytes;
            self->base.bytes_in_buffer -= num_bytes;
        }
    }
    
    static boolean resync_to_restart(j_decompress_ptr cinfo, int desired)
    {
        return jpeg_resync_to_restart(cinfo, desired);
    }
    static void term_source(j_decompress_ptr cinfo)
    {
        jpeg_istream_mgr *self = (jpeg_istream_mgr*)cinfo->src;
        delete[] self->buffer;
        self->buffer = 0;
    }

    jpeg_istream_mgr(std::istream &in_) {
        in = &in_;
        buffer = 0;
        
        base.init_source = init_source;
        base.fill_input_buffer = fill_input_buffer;
        base.skip_input_data = skip_input_data;
        base.resync_to_restart = resync_to_restart;
        base.term_source = term_source;
    }

    ~jpeg_istream_mgr() {
        delete [] buffer;
    }
    
};

struct my_error_mgr {
    struct jpeg_error_mgr pub;	/* "public" fields */

    jmp_buf setjmp_buffer;	/* for return to caller */
};

typedef struct my_error_mgr * my_error_ptr;

/*
 * Here's the routine that will replace the standard error_exit method:
 */

static void my_error_exit (j_common_ptr cinfo)
{
    /* cinfo->err really points to a my_error_mgr struct, so coerce pointer */
    my_error_ptr myerr = (my_error_ptr) cinfo->err;

    /* Always display the message. */
    /* We could postpone this until after returning, if we chose. */
    (*cinfo->err->output_message) (cinfo);

    /* Return control to the setjmp point */
    longjmp(myerr->setjmp_buffer, 1);
}


/*
 * Sample routine for JPEG decompression.  We assume that the source file name
 * is passed in.  We want to return 1 on success, 0 on error.
 */


bool imageio::jpeg::jpeg_read(std::istream &in, Image8& im, ColorSpace cs)
{
    struct jpeg_decompress_struct cinfo;
    struct my_error_mgr jerr;

    /* We set up the normal JPEG error routines, then override error_exit. */
    cinfo.err = jpeg_std_error(&jerr.pub);
    jerr.pub.error_exit = my_error_exit;

    /* Establish the setjmp return context for my_error_exit to use. */
    if (setjmp(jerr.setjmp_buffer)) {
        /* If we get here, the JPEG code has signaled an error.
         * We need to clean up the JPEG object, close the input file, and return.
         */
        jpeg_destroy_decompress(&cinfo);
        return false;
    }
    
    /* Now we can initialize the JPEG decompression object. */
    jpeg_create_decompress(&cinfo);

    /* Step 2: specify data source (eg, a file) */
    jpeg_istream_mgr src_mgr(in);
    cinfo.src = &src_mgr.base;
    
    /* Step 3: read file parameters with jpeg_read_header() */
    (void) jpeg_read_header(&cinfo, TRUE);
    
    /* Step 4: set parameters for decompression */

    switch(cs) {
    case JPEG_COLORSPACE_AUTO: break;
    case JPEG_COLORSPACE_GRAY:
        cinfo.out_color_space = JCS_GRAYSCALE;
        cinfo.output_components = 1;
        break;
    case JPEG_COLORSPACE_RGB:
        cinfo.out_color_space = JCS_RGB;
        cinfo.output_components = 3;
        break;
    }
    
    /* Step 5: Start decompressor */

    (void) jpeg_start_decompress(&cinfo);
    /* We can ignore the return value since suspension is not possible
     * with the stdio data source.
     */

    im.resize(cinfo.output_width, cinfo.output_height, cinfo.output_components);
    const int row_stride = cinfo.output_width * cinfo.output_components;
    JSAMPARRAY buffer = (*cinfo.mem->alloc_sarray)((j_common_ptr) &cinfo, JPOOL_IMAGE, row_stride, 1);
    bool success = true;
    int r=0;
    while (cinfo.output_scanline < cinfo.output_height) {
        (void)jpeg_read_scanlines(&cinfo, buffer, 1);
        memcpy(im.row(r++), buffer[0], row_stride);
    }

    /* Step 7: Finish decompression */

    (void) jpeg_finish_decompress(&cinfo);
    /* We can ignore the return value since suspension is not possible
     * with the stdio data source.
     */

    /* Step 8: Release JPEG decompression object */

    /* This is an important step since it will release a good deal of memory. */
    jpeg_destroy_decompress(&cinfo);
    
    return success;
}

struct jpeg_ostream_mgr
{
    jpeg_destination_mgr base;
    std::ostream *out;

    static const int BUF_SIZE = 1024;
    JOCTET *buffer;
    
    static void init_destination(j_compress_ptr cinfo)
    {
        jpeg_ostream_mgr *self = (jpeg_ostream_mgr*)cinfo->dest;
        self->buffer = new JOCTET[BUF_SIZE];
        self->base.next_output_byte = self->buffer;
        self->base.free_in_buffer = BUF_SIZE;
    }
    
    static boolean empty_output_buffer(j_compress_ptr cinfo)
    {        
        jpeg_ostream_mgr *self = (jpeg_ostream_mgr*)cinfo->dest;
        self->out->write((const char*)self->buffer, BUF_SIZE);
        self->base.next_output_byte = self->buffer;
        self->base.free_in_buffer = BUF_SIZE;
        return TRUE;
    }

    static void term_destination(j_compress_ptr cinfo)
    {
        jpeg_ostream_mgr *self = (jpeg_ostream_mgr*)cinfo->dest;
        int remaining = BUF_SIZE - self->base.free_in_buffer;
        if (remaining > 0) {
            self->out->write((const char*)self->buffer, remaining);
        }
        
        delete[] self->buffer;
        self->buffer = 0;
    }

    jpeg_ostream_mgr(std::ostream &out_) {
        out = &out_;
        buffer = 0;
        
        base.init_destination = init_destination;
        base.empty_output_buffer = empty_output_buffer;
        base.term_destination = term_destination;
    }
    
    ~jpeg_ostream_mgr() {
        delete[] buffer;
    }
            
};

bool imageio::jpeg::jpeg_write(const Image8& im, std::ostream& out,
                               int quality,
                               ColorSpace cs)
{
    if (im.channels != 1 && im.channels != 3)
        return false;
    
    struct jpeg_compress_struct cinfo;
    struct jpeg_error_mgr jerr;

    /* Step 1: allocate and initialize JPEG compression object */

    /* We have to set up the error handler first, in case the initialization
     * step fails.  (Unlikely, but it could happen if you are out of memory.)
     * This routine fills in the contents of struct jerr, and returns jerr's
     * address which we place into the link field in cinfo.
     */
    cinfo.err = jpeg_std_error(&jerr);
    /* Now we can initialize the JPEG compression object. */
    jpeg_create_compress(&cinfo);

    /* Step 2: specify data destination (eg, a file) */
    /* Note: steps 2 and 3 can be done in either order. */

    jpeg_ostream_mgr dst_mgr(out);
    cinfo.dest = &dst_mgr.base;

    /* Step 3: set parameters for compression */

    /* First we supply a description of the input image.
     * Four fields of the cinfo struct must be filled in:
     */
    cinfo.image_width = im.width; 
    cinfo.image_height = im.height;
    cinfo.input_components = im.channels;
    cinfo.in_color_space = im.channels == 1 ? JCS_GRAYSCALE : JCS_RGB;

    /* Now use the library's routine to set default compression parameters.
     * (You must set at least cinfo.in_color_space before calling this,
     * since the defaults depend on the source color space.)
     */
    jpeg_set_defaults(&cinfo);
    
    /* Now you can set any non-default parameters you wish to.
     * Here we just illustrate the use of quality (quantization table) scaling:
     */
    jpeg_set_quality(&cinfo, quality, TRUE /* limit to baseline-JPEG values */);
    
    switch (cs) {
    case JPEG_COLORSPACE_AUTO: break;
    case JPEG_COLORSPACE_GRAY:
        jpeg_set_colorspace(&cinfo, JCS_GRAYSCALE);
        break;
    case JPEG_COLORSPACE_RGB:
        if (im.channels != 3)
            return false;
        jpeg_set_colorspace(&cinfo, JCS_RGB);
        break;
    }

    /* Step 4: Start compressor */

    /* TRUE ensures that we will write a complete interchange-JPEG file.
     * Pass TRUE unless you are very sure of what you're doing.
     */
    jpeg_start_compress(&cinfo, TRUE);

    /* Step 5: while (scan lines remain to be written) */
    /*           jpeg_write_scanlines(...); */

    for (int r=0; r<im.height; ++r) {
        unsigned char *buffer[1] = { const_cast<unsigned char*>(im.row(r)) };
        (void) jpeg_write_scanlines(&cinfo, buffer, 1);
    }

    /* Step 6: Finish compression */

    jpeg_finish_compress(&cinfo);

    /* Step 7: release JPEG compression object */
    /* This is an important step since it will release a good deal of memory. */
    jpeg_destroy_compress(&cinfo);
    return true;
}
