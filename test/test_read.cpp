#include <fstream>
#include <cassert>
#include <iostream>

#include <imageio/jpeg.hpp>
#include <imageio/pnm.hpp>
#include <imageio/png.hpp>
#include <imageio/auto.hpp>
#include <string>

using std::cerr;
using std::cout;
using std::endl;

using namespace imageio;

int main(int argc, char *argv[])
{
    assert(argc > 1);    
    
    std::string filename = argv[1];
    Image8 im;

    if (!load(filename.c_str(), im)) {
        cerr << "Error reading " << argv[1] << endl;
        return 1;
    }

    cerr << im.width << "x" << im.height << " x " << im.channels << endl;

    const char *outname = argc > 2 ? argv[2] : "out.png";
    if (!save(im, outname)) {
        cerr << "Error writing " << outname << endl;
    }
    
    im.dealloc();
    return 0;
}
